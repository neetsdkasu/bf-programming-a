# BF-Programming-A
BrainF**kのコード書きたいけど


##例えば
```
; BF-Programming-A のコード
LET A = 3
LET B = 5
LET C = A + B
PRINT_DIGIT(C)
```
これは`A`のメモリの値を`3`、`B`のメモリの値を`5`、`A`と`B`のメモリは値を保持しながら`C`のメモリに`A`と`B`の和の値を代入する。  
メモリ0番地をA、1番地をB、2番地をC、3,4番地を一時領域(ここではS,T)として自動でコードを作ってくれるとか。  
もしこれを**BF-Programming-M**[ https://bitbucket.org/neetsdkasu/bf-programming-m ]で書くと下記のようになる  
```
; BF-Programming-M のコード
MACRO &MOVE(X)  ; X: pos to move to
  WHILE
    FILL >X,<X,3,+0,+1,-1
  WEND
END
MACRO &ADD(X,T)   ; X: pos to add to, T: temporary pos from X
  INSERT &MOVE((X+T))
  PTR >(X+T)
  WHILE
    FILL <(X+T),>X,>T,4,+0,+1,+1,-1
  WEND
END
MACRO &PRINT_DIGIT()
  VAL +'0'
  OUTPUT
  VAL -'0'
END
CONST A=0     ; メモリ上の位置を表す
CONST B=1
CONST C=2
CONST S=3     ; 計算式の途中結果を保持するための一時領域
CONST T=4     ; AやBの値の回復用の一時領域
PTR >A        ; -> A
VAL 3         ; A = 3
PTR >(B-A)    ; A -> B
VAL 5         ; B = 5
PTR >(C-B)    ; B -> C
VAL 0         ; C = 0
VAL >(S-C)    ; C -> S
VAL 0         ; S = 0
PTR >(T-S)    ; S -> T
VAL 0         ; T = 0
PTR <(T-A)    ; T -> A
INSERT &ADD((S-A),(T-S))  ; S += A
PTR <(T-B)                ; T -> B
INSERT &ADD((S-B),(T-S))  ; S += B
PTR <(T-S)                ; T -> S
INSERT &MOVE((-(S-C)))    ; C = S, S = 0
PTR <(S-C)                ; S -> C
INSERT &PRINT_DIGIT()     ; 
```


##例えば
```
; BF-Programming-A のコード
GET_DIGIT(A)
IF A == 0 THEN
  LET B = 3
ELSE
  LET C = 5
END IF
```
`A`が`0`かどうかによって`B`や`C`の値を決める。  
もしこれを**BF-Programming-M**で書くと例えば下記のようになる 。 
```
; BF-Programming-M のコード
MACRO &GET_DIGIT()
  VAL 0
  INPUT
  VAL -'0'
END
CONST A=0   ; メモリ上の位置を表す
CONST B=1
CONST C=2
CONST F=3   ; Fは条件分岐の判定フラグ
CONST T=4   ; TはAの値を保持するための一時領域
PTR >F      ; -> F
VAL 1       ; F = 1
PTR >(T-F)  ; F -> T
VAL 0       ; T = 0
PTR <(T-A)  ; T -> A
INSERT &GET_DIGIT()
WHILE           ; A > 0
  PTR >(F-A)    ; A -> F
  WHILE         ; F > 0
    VAL -1      ; F -= 1 (F == 0) ... A!=0ならF=0に
    PTR <(F-C)  ; F -> C
    VAL = 5     ; C = 5   ... ELSE(A!=0)の処理
    PTR >(F-C)  ; C -> F
  WEND
  PTR >(T-F)    ; F -> T
  VAL +1        ; T += 1  ... AをTにコピー 
  PTR <(T-A)    ; T -> A
  VAL -1        ; A -= 1
WEND
PTR >(T-A)      ; A -> T
WHILE           ; Aの回復
  FILL <(T-A),>(T-A),3,+0,+1,-1
WEND
PTR <(T-F)      ; T -> F
WHILE           ; F > 0   ... A==0ならF==1となる、A!=0なら先のループ内でF=0になる
  PTR <(F-B)    ; F -> B
  VAL 3         ; B = 3   ... THEN(A==0)の処理
  PTR >(F-B)    ; B -> F
  VAL -1        ; F -= 1 (F == 0)
WEND
```


##例えば
```
; BF-Programming-A のコード
LET A = 30
```
を`+`30個じゃなくループ駆使して生成するとか(ソースコードのダイエット)。  
実行速度面ではループを使わない`+`を30個並べるほうが速いと思う。  
```
; BF-Programming-M のコード

; +を30個  => [-] ++++++++++ ++++++++++ ++++++++++
CONST A=0
PTR >A
VAL 30

; ループ駆使 => [-]>[-] +++++[<++++++>-]
CONST A=0
CONST T=1
PTR >A
VAL 0
PTR >(T-A)
VAL 5
WHILE
  FILL <(T-A),>(T-A),3,+0,+6,-1
WEND

```

